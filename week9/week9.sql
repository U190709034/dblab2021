select * from Customers;

update Customers set country=replace(country, '\n', '');
update Customers set city=replace(city, '\n', '');

create view mexicancustomers as 
select CustomerID, CustomerName, ContactName
from Customers
where country="Mexico";

select * from mexicancustomers;

select * 
from mexicancustomers join Orders on mexicancustomers.CustomerID = Orders.CustomerID;

create view productsbelowavg as
select ProductID, ProductName, Price
from Products
where Price < (select avg(Price) from Products);

delete from OrderDetails where ProductID=5;
truncate OrderDetails; 

delete from Customers;
delete from Orders;

drop table Customers;